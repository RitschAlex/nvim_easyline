local api = vim.api
local fn = vim.fn

local M = {}

local modes = {
    ["n"] = "NORMAL",
    ["no"] = "NORMAL",
    ["v"] = "VISUAL",
    ["V"] = "VISUAL LINE",
    ["^V"] = "VISUAL BLOCK",
    ["s"] = "SELECT",
    ["S"] = "SELECT LINE",
    ["^S"] = "SELECT BLOCK",
    ["i"] = "INSERT",
    ["ic"] = "INSERT",
    ["R"] = "REPLACE",
    ["Rv"] = "VISUAL REPLACE",
    ["c"] = "COMMAND",
    ["cv"] = "VIM EX",
    ["ce"] = "EX",
    ["r"] = "PROMPT",
    ["rm"] = "MOAR",
    ["r?"] = "CONFIRM",
    ["!"] = "SHELL",
    ["t"] = "TERMINAL"
}

local function mode()
    local current_mode = api.nvim_get_mode().mode
    return string.format(" %s ", modes[current_mode]):upper()
end

local mode_colors = {
    n = "%#StatusLineAccent#",
    i = "%#StatusLineInsertAccent#",
    ic = "%#StatusLineInsertAccent#",
    v = "%#StatusLineVisualAccent#",
    V = "%#StatusLineVisualAccent#",
    ["^V"] = "%#StatusLineVisualAccent#",
    R = "%#StatusLineReplaceAccent#",
    c = "%#StatusLineCmdLineAccent#",
    t = "%#StatusLineTerminalAccent#"
}

local function update_mode_colors()
    local current_mode = api.nvim_get_mode().mode
    return mode_colors[current_mode] or "%#StatusLineAccent#"
end

local function filepath()
    local fpath = fn.fnamemodify(fn.expand("%"), ":~:.:h")
    return fpath == " " or fpath == "." and " " or
               string.format(" %%<%s/", fpath)
end

local function filename()
    local fname = fn.expand("%:t")
    return fname == "" and "" or fname .. " "
end

local function filetype() return string.format(" %s ", vim.bo.filetype):upper() end

local function lineinfo()
    return vim.bo.filetype == "alpha" and "" or " %P %l:%c "
end

local function lsp()
    local diagnostics = {"error", "warn", "hint", "info"}
    local symbols = {error = "E", warn = "W", hint = "H", info = "I"}
    local result = {}

    for _, diag in ipairs(diagnostics) do
        local count = vim.tbl_count(vim.diagnostic.get(0, {
            severity = diag:upper()
        }))
        if count > 0 then
            table.insert(result,
                         string.format("%%#LspDiagnosticsSign%s# %s:%d ",
                                       diag:sub(1, 1):upper() .. diag:sub(2),
                                       symbols[diag], count))
        end
    end
    return "%=" .. table.concat(result, "")
end

local function active_statusline()
    return table.concat({
        update_mode_colors(), mode(), "%#StatusLineFile#", filepath(),
        filename(), "%#StatusLineFile#", lsp(), "%#StatusLineExtra#",
        filetype(), lineinfo()
    })
end

local function short_statusline() return "%#StatusLineNC#   NvimTree" end

local function inactive_statusline() return " %F " end

local function setup_autocommands()
    api.nvim_exec([[
      augroup EasyLine
      au!
      au VimEnter,WinEnter,BufEnter * setlocal statusline=%!v:lua.require('statusline').active()
      au WinLeave,BufLeave * setlocal statusline=%!v:lua.require('statusline').inactive()
      au WinEnter,BufEnter,FileType NvimTree setlocal statusline=%!v:lua.require('statusline').short()
      augroup END
    ]], false)
end

function M.setup() setup_autocommands() end

M.active = active_statusline
M.inactive = inactive_statusline
M.short = short_statusline

return M
